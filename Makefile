SHELL := /bin/zsh

tests:
	symfony console doctrine:fixtures:load -n
	symfony php bin/phpunit

clear:
	symfony console doctrine:database:drop --force -n
	symfony console doctrine:database:create -n
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load -n

.PHONY: tests