<?php

namespace App\EntityListener;

use App\Entity\Survey;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class SurveyEntityListener
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Survey $survey)
    {
        $survey->computeSlug($this->slugger);
    }

    public function preUpdate(Survey $survey)
    {
        $survey->computeSlug($this->slugger);
    }
}