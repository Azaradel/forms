<?php

namespace App\Controller;

use App\Entity\Survey;
use App\Entity\SurveyQuestion;
use App\Entity\SurveyResponse;
use App\Form\SurveyFormType;
use App\Form\SurveyQuestionFormType;
use App\Form\SurveyResponseFormType;
use App\Repository\SurveyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;

class SurveyController extends AbstractController
{
    private $logger;
    private $twig;
    private $entityManager;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, SurveyRepository $surveyRepository)
    {
        $survey = new Survey();
        $form = $this->createForm(SurveyFormType::class, $survey);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->persist($survey);
                $this->entityManager->flush();
            } catch(Exception $exception) {
                $this->logger->error('survey ' . $survey->getSlug() . ' already exists');
            }
            return $this->redirectToRoute('survey', ['slug'=> $survey->getSlug()]);
        }

        return new Response($this->twig->render('survey/index.html.twig', [
            'surveys' => $surveyRepository->findAll(),
            'survey_form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/survey/{slug}", name="survey", requirements={"slug":"[a-z\-]+"})
     */
    public function show(Survey $survey)
    {
        return new Response($this->twig->render('survey/show.html.twig', [
            'survey' => $survey,
        ]));
    }

    /**
     * @Route("/survey/{slug}/edit", name="survey_edit", requirements={"slug":"[a-z\-]+"})
     */
    public function edit(Request $request, Survey $survey)
    {
        $form = $this->createForm(SurveyFormType::class, $survey);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('survey', ['slug'=> $survey->getSlug()]);
        }

        return new Response($this->twig->render('survey/edit.html.twig', [
            'survey' => $survey,
            'survey_form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/survey/{slug}/take", name="survey_take")
     */
    public function take(Request $request, Survey $survey) {
        if(!$survey->getIsPublished()) {
            return new Response($this->twig->render('not_found.html.twig'));
        }

        $surveyResponse = new SurveyResponse();
        $form = $this->createForm(SurveyResponseFormType::class, null, [
            'survey' => $survey
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            foreach ($data as $key=>$answer) {
                $surveyResponse->addAnswer($answer);
            }

            $this->entityManager->persist($surveyResponse);
            $this->entityManager->flush();

            $this->redirectToRoute('homepage');
        }

        return new Response($this->twig->render('survey/take.html.twig', [
            'survey' => $survey,
            'survey_response_form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/survey/{slug}/delete", name="survey_delete")
     */
    public function delete(Request $request, Survey $survey)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY', $request->getUser())) {
            $this->entityManager->remove($survey);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('homepage');
    }
}
