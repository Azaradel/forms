<?php

namespace App\Controller;

use App\Entity\AbstractAnswer;
use App\Entity\AnswerChoice;
use App\Entity\Survey;
use App\Entity\SurveyQuestion;
use App\Form\AnswerFormType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/survey/{slug}/question/{id}/answer")
 */
class AnswerController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{answer}", name="delete_answer")
     */
    public function delete(Survey $survey, AnswerChoice $answer)
    {
        $this->entityManager->remove($answer);
        $this->entityManager->flush();

        return $this->redirectToRoute('survey', ['slug' => $survey->getSlug()]);
    }

    /**
     * @Route("/", name="add_answer")
     */
    public function add(Request $request, SurveyQuestion $question)
    {
        $answer = new AnswerChoice();
        $form = $this->createForm(AnswerFormType::class, $answer);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $question->getAnswer()->addSelectedChoice($answer);
            $answer->setValue(false);
            $this->entityManager->persist($question);
            $this->entityManager->flush();

            return $this->redirectToRoute('survey', ['slug' => $question->getSurvey()->getSlug()]);
        }

        return $this->render('answer/index.html.twig', [
            'answer_form' => $form->createView(),
        ]);
    }


}
