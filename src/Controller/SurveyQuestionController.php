<?php

namespace App\Controller;

use App\Entity\Survey;
use App\Entity\SurveyQuestion;
use App\Form\SurveyQuestionFormType;
use App\Repository\SurveyQuestionRepository;
use App\Repository\SurveyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @Route("/survey/{slug}/question", requirements={"slug":"[a-z\-]+"})
 */
class SurveyQuestionController extends AbstractController
{
    private $twig;
    private $entityManager;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/edit", name="survey_question_edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request, SurveyQuestion $question)
    {
        $form = $this->createForm(SurveyQuestionFormType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('survey', ['slug'=> $question->getSurvey()->getSlug()]);
        }

        return new Response($this->twig->render('survey_question/index.html.twig', [
            'question' => $question,
            'question_form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/", name="survey_question_create")
     */
    public function create(Request $request, Survey $survey)
    {
        $question = new SurveyQuestion();
        $question->setText('');
        $form = $this->createForm(SurveyQuestionFormType::class, $question);
        $form->handleRequest($request);
        $question->setSurvey($survey);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($question);
            $this->entityManager->flush();

            return $this->redirectToRoute('survey', ['slug'=> $survey->getSlug()]);
        }

        return new Response($this->twig->render('survey_question/index.html.twig', [
            'question' => $question,
            'question_form' => $form->createView(),
        ]));
    }
}
