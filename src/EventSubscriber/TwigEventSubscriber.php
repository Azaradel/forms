<?php

namespace App\EventSubscriber;

use App\Repository\SurveyRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Twig\Environment;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $surveyRepository;

    public function __construct(Environment $twig, SurveyRepository $surveyRepository)
    {
        $this->twig = $twig;
        $this->surveyRepository = $surveyRepository;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $this->twig->addGlobal('surveys', $this->surveyRepository->findAll());
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.controller' => 'onKernelController',
        ];
    }
}
