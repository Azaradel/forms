<?php

namespace App\Entity;

use App\Repository\AnswerChoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnswerChoiceRepository::class)
 */
class AnswerChoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $key;

    /**
     * @ORM\Column(type="boolean", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=CheckboxAnswer::class, inversedBy="selectedChoices")
     */
    private $checkboxAnswer;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getValue(): ?bool
    {
        return $this->value;
    }

    public function setValue(bool $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCheckboxAnswer(): ?CheckboxAnswer
    {
        return $this->checkboxAnswer;
    }

    public function setCheckboxAnswer(?CheckboxAnswer $checkboxAnswer): self
    {
        $this->checkboxAnswer = $checkboxAnswer;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getKey();
    }
}
