<?php

namespace App\Entity;

use App\Repository\AbstractAnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @InheritanceType("SINGLE_TABLE")
 *
 * @ORM\Table(name="answer")
 * @ORM\DiscriminatorMap({
 *     "checkbox" = "App\Entity\CheckboxAnswer",
 *     "text" = "App\Entity\TextAnswer",
 * })
 * @ORM\Entity(repositoryClass=AbstractAnswerRepository::class)
 */
abstract class AbstractAnswer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=SurveyQuestion::class, inversedBy="answer", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuestion(): ?SurveyQuestion
    {
        return $this->question;
    }

    public function setQuestion(SurveyQuestion $question): self
    {
        $this->question = $question;

        return $this;
    }
}
