<?php

namespace App\Entity;

use App\Repository\TextAnswerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TextAnswerRepository::class)
 */
class TextAnswer extends AbstractAnswer
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $answer;

    /**
     * @ORM\Column(type="boolean")
     */
    private $textArea;


    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getTextArea(): ?bool
    {
        return $this->textArea;
    }

    public function setTextArea(bool $textArea): self
    {
        $this->textArea = $textArea;

        return $this;
    }

    public function __toString()
    {
        return $this->getAnswer();
    }
}
