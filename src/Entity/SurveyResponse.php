<?php

namespace App\Entity;

use App\Repository\SurveyResponseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SurveyResponseRepository::class)
 */
class SurveyResponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Survey::class, inversedBy="surveyResponses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $survey;

    /**
     * @ORM\ManyToOne(targetEntity=AbstractAnswer::class)
     */
    private $answers;

    /**
     * @ORM\Column(type="datetime")
     */
    private $submittedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }

    public function getAnswers(): ?AbstractAnswer
    {
        return $this->answers;
    }

    public function setAnswers(?AbstractAnswer $answers): self
    {
        $this->answers = $answers;

        return $this;
    }

    public function addAnswer(?AbstractAnswer $answer): self
    {
        array_push($this->answers, $answer->getId(), $answer);

        return $this;
    }

    public function getSubmittedAt(): ?\DateTimeInterface
    {
        return $this->submittedAt;
    }

    public function setSubmittedAt(\DateTimeInterface $submittedAt): self
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }
}
