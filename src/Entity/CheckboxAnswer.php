<?php

namespace App\Entity;

use App\Repository\CheckboxAnswerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CheckboxAnswerRepository::class)
 */
class CheckboxAnswer extends AbstractAnswer
{
    /**
     * @ORM\OneToMany(targetEntity=AnswerChoice::class, mappedBy="checkboxAnswer", cascade={"persist", "remove"})
     */
    private $selectedChoices;

    public function __construct()
    {
        $this->selectedChoices = new ArrayCollection();
    }

    /**
     * @return Collection|AnswerChoice[]
     */
    public function getSelectedChoices(): Collection
    {
        return $this->selectedChoices;
    }

    public function addSelectedChoice(AnswerChoice $selectedChoice): self
    {
        if (!$this->selectedChoices->contains($selectedChoice)) {
            $this->selectedChoices[] = $selectedChoice;
            $selectedChoice->setCheckboxAnswer($this);
        }

        return $this;
    }

    public function removeSelectedChoice(AnswerChoice $selectedChoice): self
    {
        if ($this->selectedChoices->contains($selectedChoice)) {
            $this->selectedChoices->removeElement($selectedChoice);
            // set the owning side to null (unless already changed)
            if ($selectedChoice->getCheckboxAnswer() === $this) {
                $selectedChoice->setCheckboxAnswer(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getQuestion();
    }
}
