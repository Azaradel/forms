<?php

namespace App\Entity;

use App\Repository\SurveyQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SurveyQuestionRepository::class)
 */
class SurveyQuestion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRequired;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=Survey::class, inversedBy="surveyQuestion")
     */
    private $survey;

    /**
     * @ORM\OneToOne(targetEntity=AbstractAnswer::class, mappedBy="question", cascade={"persist", "remove"})
     */
    private $answer;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsRequired(): ?bool
    {
        return $this->isRequired;
    }

    public function setIsRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }

    public function __toString(): string
    {
        return $this->text;
    }

    public function getAnswer(): ?AbstractAnswer
    {
        return $this->answer;
    }

    public function setAnswer(AbstractAnswer $answer): self
    {
        $this->answer = $answer;

        // set the owning side of the relation if necessary
        if ($answer->getQuestion() !== $this) {
            $answer->setQuestion($this);
        }

        return $this;
    }
}
