<?php

namespace App\Twig;

use App\Entity\CheckboxAnswer;
use App\Entity\TextAnswer;
use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class AppExtension extends AbstractExtension
{
    public function getTests()
    {
        return [
            new TwigTest('textAnswer', [$this, 'isTextAnswer']),
            new TwigTest('checkboxAnswer', [$this, 'isCheckboxAnswer']),
        ];
    }

    public function isTextAnswer($var) {
        return $var instanceof TextAnswer;
    }

    public function isCheckboxAnswer($var) {
        return $var instanceof CheckboxAnswer;
    }
}