<?php

namespace App\Form;

use App\Entity\CheckboxAnswer;
use App\Entity\SurveyQuestion;
use App\Entity\TextAnswer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyQuestionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_required', CheckboxType::class, [
                'required' => false,
            ])
            ->add('text')
            ->add('answer', ChoiceType::class, [
                'choices' => [
                    'Text' => new TextAnswer(),
                    'Checkbox' => new CheckboxAnswer(),
                ]
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SurveyQuestion::class,
        ]);
    }
}
