<?php

namespace App\Form;

use App\Entity\AbstractAnswer;
use App\Entity\CheckboxAnswer;
use App\Entity\Survey;
use App\Entity\SurveyResponse;
use App\Entity\TextAnswer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyResponseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $survey = $options['survey'];
        $questions = $survey->getSurveyQuestion();

        foreach ($questions as $key=>$question) {
            $answer = $question->getAnswer();
            if ($answer instanceof TextAnswer) {
                $builder->add('answer'.$answer->getId(), TextType::class);
            } elseif ($answer instanceof CheckboxAnswer) {
                $array = [];
                foreach ($answer->getSelectedChoices() as $choice) {
                    array_push($array, $choice->getKey(), $choice);
                }

                $builder->add('answer'.$answer->getId(), ChoiceType::class, [
                    'choices' => $array,
                ]);
            }
        }

        $builder
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'survey' => Survey::class
        ]);
    }
}
