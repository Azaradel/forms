<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\AnswerChoice;
use App\Entity\CheckboxAnswer;
use App\Entity\TextAnswer;
use App\Entity\Survey;
use App\Entity\SurveyQuestion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Query\AST\InExpression;
use SebastianBergmann\CodeCoverage\Report\Text;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AppFixtures extends Fixture
{
    private $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function load(ObjectManager $manager)
    {
        $survey1 = new Survey();
        $survey1->setIsPublished(true);
        $survey1->setName('Programming languages and IDEs');
        $manager->persist($survey1);

        $question1 = new SurveyQuestion();
        $question1->setText('Which programming language is primary for you?');
        $question1->setIsRequired(true);
        $question1->setSurvey($survey1);
        $manager->persist($question1);

        $checkboxAnswer = new CheckboxAnswer();
        $answerChoice1 = new AnswerChoice();
        $answerChoice1->setKey('Go');
        $answerChoice1->setValue(false);
        $manager->persist($answerChoice1);

        $checkboxAnswer->addSelectedChoice($answerChoice1);
        $answerChoice2 = new AnswerChoice();
        $answerChoice2->setKey('PHP');
        $answerChoice2->setValue(false);
        $manager->persist($answerChoice2);
        $checkboxAnswer->addSelectedChoice($answerChoice2);

        $answerChoice3 = new AnswerChoice();
        $answerChoice3->setKey('Javascript');
        $answerChoice3->setValue(false);
        $manager->persist($answerChoice3);
        $checkboxAnswer->addSelectedChoice($answerChoice3);

        $answerChoice4 = new AnswerChoice();
        $answerChoice4->setKey('Rust');
        $answerChoice4->setValue(false);
        $manager->persist($answerChoice4);
        $checkboxAnswer->addSelectedChoice($answerChoice4);

        $checkboxAnswer->setQuestion($question1);
        $manager->persist($checkboxAnswer);

        $question2 = new SurveyQuestion();
        $question2->setText('Why did you choose this language?');
        $question2->setIsRequired(false);
        $question2->setSurvey($survey1);
        $manager->persist($question2);

        $textAnswer1 = new TextAnswer();
        $textAnswer1->setTextArea(true);
        $textAnswer1->setQuestion($question2);
        $manager->persist($textAnswer1);

        $survey2 = new Survey();
        $survey2->setIsPublished(false);
        $survey2->setName('Fast food choices and preferences');
        $manager->persist($survey2);

        $question5 = new SurveyQuestion();
        $question5->setText('How often do you eat junk food?');
        $question5->setIsRequired(true);
        $question5->setSurvey($survey2);
        $manager->persist($question5);

        $checkboxAnswer = new CheckboxAnswer();
        $answerChoice1 = new AnswerChoice();
        $answerChoice1->setKey('once a week');
        $answerChoice1->setValue(false);
        $checkboxAnswer->addSelectedChoice($answerChoice1);

        $answerChoice2 = new AnswerChoice();
        $answerChoice2->setKey('everyday');
        $answerChoice2->setValue(false);
        $checkboxAnswer->addSelectedChoice($answerChoice2);

        $answerChoice3 = new AnswerChoice();
        $answerChoice3->setKey('few times a week');
        $answerChoice3->setValue(false);
        $checkboxAnswer->addSelectedChoice($answerChoice3);

        $checkboxAnswer->setQuestion($question5);
        $manager->persist($checkboxAnswer);

        $admin = new Admin();
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setUsername('admin');
        $admin->setPassword($this->encoderFactory->getEncoder(Admin::class)->encodePassword('admin', null));
        $manager->persist($admin);

        $manager->flush();
    }
}
