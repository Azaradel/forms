<?php

namespace App\Repository;

use App\Entity\CheckboxAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CheckboxAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method CheckboxAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method CheckboxAnswer[]    findAll()
 * @method CheckboxAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CheckboxAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CheckboxAnswer::class);
    }

    // /**
    //  * @return CheckboxAnswer[] Returns an array of CheckboxAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CheckboxAnswer
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
